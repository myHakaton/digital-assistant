Реализованная функциональность
- помощь в навигации по ресурсу с подсказками 
- примеры формулировок для заполнения полей
- выбор данных из предыдущих заявок
- уведомления

Особенность проекта в следующем:
- вызов подсказок голосом
- озвучка навигации и подсказок
- валидация полей включая загружаемую фотографию, с корректировкой до нужных параметоров

Основной стек технологий:
TypeScript, JavaScript, HTML, CSS,
Vite, React,
Git, Gitlab.

Демо
User script для браузера доступен по адресу: https://github.com/9yx/hackUserScrypt

СРЕДА ЗАПУСКА
запускается в браузере Chrome 108 c установленным расширением "User JavaScript and CSS" 
(https://chrome.google.com/webstore/detail/user-javascript-and-css/nbhcbdghjpllgmfilhnhkllmkecfmpld )

Сборка проекта
npm run  build
tsc && vite build",
vite preview --host

РАЗРАБОТЧИКИ
Акмал Комалов https://t.me/volamok
Рустам Салатхудинов https://t.me/SalahutdinovRS
Евгений Зайков https://t.me/RaShare
Иван Гуляев https://t.me/googlaev
Мезенцев Руслан https://t.me/runx0
