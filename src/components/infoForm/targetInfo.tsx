import React from "react";
import {Typography} from "antd";

export const TargetInfo: React.FC<{ title: string }> = () => {
    return (
        <Typography>
            <Typography.Title level={5}> При описании цели придерживайтесь техники SMART:</Typography.Title>
            <Typography.Paragraph>
                <Typography.Text strong style={{color:"#faa419"}}>Конкретной (Specific)</Typography.Text> — точно обозначать действия, которые нужно сделать.
                Пример: «Провести маркетинговую кампанию, посвященную новой марке зубной пасты, в социальных сетях с
                охватом 1 млн человек».
            </Typography.Paragraph>
            <Typography.Paragraph>
                <Typography.Text strong style={{color:"#faa419"}}>Измеримой (Measurable)</Typography.Text> — отражать масштаб работ в количественном выражении. Это поможет понять, когда цель будет достигнута.
                Пример: «Повысить количество просмотров видеоролика на YouTube до 1 млн».
            </Typography.Paragraph>
            <Typography.Paragraph>
                <Typography.Text strong style={{color:"#faa419"}}>Достижимой (Attainable)</Typography.Text> — рассчитанной на те ресурсы, которые есть в распоряжении исполнителя.
                Пример: «Написать первую главу книги за одну неделю».
            </Typography.Paragraph>
            <Typography.Paragraph>
                <Typography.Text strong style={{color:"#faa419"}}>Актуальной (Relevant)</Typography.Text> — соответствующей целям постановщика задачи и здравому смыслу.
                Пример: «Изучить аудиторию с „Одноклассников“ и написать посты для этой социальной сети, чтобы привлечь
                новых пользователей».
            </Typography.Paragraph>
            <Typography.Paragraph>
                <Typography.Text strong style={{color:"#faa419"}}>Ограниченной по времени (Time Based)</Typography.Text> — задающей конкретные сроки.
                Пример: «Подготовить презентацию до 01.01.2021 включительно».
            </Typography.Paragraph>
        </Typography>
    )
}