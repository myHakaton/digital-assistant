import React, {useState} from "react";
import {Popover, Space, Tabs, Upload} from "antd";
import {Button} from "../button";
import {SaveOutlined} from "@ant-design/icons";
import Resizer from "react-image-file-resizer";
import {RcFile} from "antd/es/upload";
import {b64ToBlob} from "../../utils/b64ToBlob";
// @ts-expect-error https://github.com/onurzorluer/react-image-file-resizer/issues/68
const resizer: typeof Resizer = (Resizer.default || Resizer);
export const ImageInfo: React.FC = () => {
    const [] = useState("");
    return (
        <Upload
            fileList={[]}
            beforeUpload={async (file: RcFile) => {
                try {
                    resizer.imageFileResizer(
                        file,
                        300,
                        300,
                        "JPEG",
                        100,
                        0,
                        (file) => {
                            const url = window.URL.createObjectURL(b64ToBlob(file as string));
                            window.open(url);
                        },
                        "base64",
                        200,
                        200
                    );
                } catch (err) {
                    console.log(err);
                }
                return false
            }}
        >
            <Button
                type={"dashed"}
                icon={<SaveOutlined/>}
            >
                Подготовить фото
            </Button>
        </Upload>
    )
}