import React from "react";
import {Space, Typography} from "antd";

export const InfoEmail: React.FC<{ title: string, fields: { label: string, value: any }[] }> = (props) => {
    return (
        <Space direction={"vertical"}>
            {props.fields.filter(item => item.label === props.title && item.value).map(item => (
                <Typography.Text copyable={{tooltips: false}}>
                    {item.value}
                </Typography.Text>
            ))}
        </Space>
    )
}