import React from "react";
import {Button as AntButton, ConfigProvider} from "antd";
import {ButtonProps} from "antd/es/button/button";

export const Button: React.FC<ButtonProps> = (props) => {
    return (
        <ConfigProvider
            theme={{
                token: {
                    colorPrimary: "#faa419",
                    colorPrimaryHover: "#e19417",
                    colorTextLightSolid: "#fff",
                    colorBorder: "#faa419",
                    colorText: "#faa419"
                }
            }}
        >
            <AntButton {...props}/>
        </ConfigProvider>
    )
}