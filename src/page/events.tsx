import React, {useEffect} from "react";
import {FileProtectOutlined, InfoOutlined} from "@ant-design/icons";
import {useSetRecoilState} from "recoil";
import {currentStepHelp, startingStatusHelp} from "../state/state.atoms";
import {useInit} from "../state/state.hooks";
import {apiGetEvents, apiGetProjects} from "../api/api.get";
import ReactDOM from "react-dom/client";
import {Button} from "../components/button";

export const EventsPage: React.FC = () => {
    const setStartingTour = useSetRecoilState(startingStatusHelp);
    const setCurrent = useSetRecoilState(currentStepHelp);
    const init = useInit();
    useEffect(() => {
        init();
    }, []);
    useEffect(() => {
        apiGetEvents().then((response: any[]) => {
            response.forEach(event => {
                if (event.grants.length > 0 && event.grants[0].projectTemplate) {
                    const projectTemplate = event.grants[0].projectTemplate;
                    return apiGetProjects().then(projects => {
                        console.log(projects);
                        setTimeout(function () {
                            const element = document.querySelectorAll(`[href*="/events/${event.ID}"]`)[0];
                            const root = document.createElement('div');
                            root.style.display = "flex";
                            root.style.justifyContent = "space-between";
                            element.parentElement?.appendChild(root);
                            ReactDOM.createRoot(root).render(
                                <React.Fragment>
                                    <Button
                                        type={"primary"}
                                        size={"large"}
                                        onClick={() => {
                                            location.href = `/projects/create/${projectTemplate.typeID}:${projectTemplate.ID}`

                                        }}
                                    >
                                        Создать проект
                                    </Button>
                                    <Button
                                        size={"large"}
                                        icon={<FileProtectOutlined />}
                                        onClick={() => {
                                            location.href = `/projects/create/${projectTemplate.typeID}:${projectTemplate.ID}`

                                        }}
                                    />
                                </React.Fragment>
                            );
                        }, 500);
                    })
                }
            });
        })
    }, []);
    return (
        <Button
            type={"primary"}
            shape="circle"
            size={"large"}
            icon={<InfoOutlined/>}
            onClick={() => {
                setCurrent(1);
                setStartingTour(true);
            }}
        />
    )
}
