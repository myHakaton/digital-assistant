import React, {useEffect} from "react";
import {useSetRecoilState} from "recoil";
import {currentStepHelp, startingStatusHelp} from "../state/state.atoms";
import {useInit} from "../state/state.hooks";
import {Button} from "antd";
import {InfoOutlined} from "@ant-design/icons";

export const EventPage: React.FC = () => {
    const setStartingTour = useSetRecoilState(startingStatusHelp);
    const setCurrent = useSetRecoilState(currentStepHelp);
    const init = useInit();
    useEffect(() => {
        init();
    }, []);
    return (
        <Button
            type={"primary"}
            shape="circle"
            size={"large"}
            icon={<InfoOutlined/>}
            onClick={() => {
                setCurrent(3);
                setStartingTour(true);
            }}
        />
    )
}