import React, {useEffect, useState} from "react";
import {InfoOutlined} from "@ant-design/icons";
import {Button, Popconfirm, Tooltip} from "antd";
import {useSetRecoilState} from "recoil";
import {currentStepHelp, startingStatusHelp} from "../state/state.atoms";
import {useInit} from "../state/state.hooks";

const text = [
    "Привет, я голосовой помощник.",
    "Если тебе вдруг понадобиться моя помощь, обращайся."
];

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export const RootPage: React.FC = () => {
    const setStartingTour = useSetRecoilState(startingStatusHelp);
    const setCurrent = useSetRecoilState(currentStepHelp);
    const init = useInit();
    const [showMessage, setShowMessage] = useState(false);
    const [message, setMessage] = useState("");
    useEffect(() => {
        init();
    }, []);
    useEffect(() => {
        const timer = setTimeout(() => {
            async function demo() {
                setShowMessage(true);
                for (let txt of text) {
                    let message = "";
                    for (let letter of txt.split("")) {
                        message += letter;
                        setMessage(message);
                        await sleep(40);
                    }
                    await sleep(3000);
                }
                setShowMessage(false);
            }
            demo();
        });
        return () => clearTimeout(timer);
    }, []);
    return (
        <Tooltip
            title={<span style={{color: "#faa419", fontWeight: 600}}>{message}</span>}
            color={"white"}
            open={showMessage}
            placement={"leftTop"}
        >
            <Button
                type={"primary"}
                shape="circle"
                size={"large"}
                icon={<InfoOutlined/>}
                onClick={() => {
                    setCurrent(0);
                    setStartingTour(true);
                }}
            />
        </Tooltip>
    )
}