import React, {useEffect, useState} from "react";
import {InfoOutlined} from "@ant-design/icons";
import {Button} from "../components/button";
import {useSetRecoilState} from "recoil";
import {currentStepHelp, startingStatusHelp} from "../state/state.atoms";
import {useInit} from "../state/state.hooks";
import {RenderPromptForm} from "../utils/render-prompt-form";
import {useLocation} from "react-router";
import {InfoVideo} from "../components/infoForm/video";
import {InfoEmail} from "../components/infoForm/email";
import {apiGetProjects, apiGetProjectsForm} from "../api/api.get";
import {RenderElement} from "../utils/render-element";
import {ImageInfo} from "../components/infoForm/image";
import {HelperInfo} from "../components/infoForm/helper";
import {TargetInfo} from "../components/infoForm/targetInfo";

const formItems: [string, string][] = [
    ["Опыт руководителя", "Иванов Иван Иванович"],
    ["Адрес регистрации руководителя проекта", "г. Тюмень, ул. Габдуллы Тукая, д. 5"],
    ["Электронная почта/Email", "test@exemple.org"],
    ["Видео-визитка (ссылка на ролик на любом видеохостинге)", "https://www.youtube.com/watch?v=ipI9ApwEABg&list=PLKaafC45L_SRoYnuEW5cgqHN-kpSTVfMs&index=2"],

    ["Краткая информация о проекте", "Мой проект будет четким"],
    ["Описание проблемы, решению/снижению остроты которой посвящен проект", "Конечно экология очень запушена"],
    ["Основные целевые группы, на которые направлен проект", "Цель люди конечно же"],
    ["Основная цель проекта", '"250 миллионов рублей”, “5 семинаров в месяц”, "15% конверсии"'],
    ["Опыт успешной реализации проектов", "Развили в Тюмени экологические парки"],
    ["Перспектива развития и потенциал проекта", "В будущем только позитив"]
];

export const ProjectsCreate: React.FC = () => {
    const location = useLocation();
    const setStartingTour = useSetRecoilState(startingStatusHelp);
    const setCurrent = useSetRecoilState(currentStepHelp);
    const init = useInit();
    const [formsValues, setFormValues] = useState<{ label: string, value: any }[]>([]);
    useEffect(() => {
        init();
    }, []);
    useEffect(() => {
        const map = new Map<string, JSX.Element>([
            ["key/form-value/0", <HelperInfo title={"Опыт руководителя"}/>],
            ["key/form-value/1", <InfoEmail title={"Адрес регистрации руководителя проекта"} fields={formsValues}/>],
            ["key/form-value/2", <InfoEmail title={"Электронная почта/Email"} fields={formsValues}/>],
            ["key/form-value/3", <InfoVideo title={"Видео-визитка (ссылка на ролик на любом видеохостинге)"}/>],
            ["key/form-value/4", <HelperInfo title={"Краткая информация о проекте"}/>],
            ["key/form-value/5",
                <HelperInfo title={"Описание проблемы, решению/снижению остроты которой посвящен проект"}/>],
            ["key/form-value/6", <HelperInfo title={"Основные целевые группы, на которые направлен проект"}/>],
            ["key/form-value/7", <TargetInfo title={"Основная цель проекта"}/>],
            ["key/form-value/8", <HelperInfo title={"Опыт успешной реализации проектов"}/>],
            ["key/form-value/9", <HelperInfo title={"Перспектива развития и потенциал проекта"}/>],
            ["key/form-value/10", <HelperInfo title={"Электронная почта/Email1"}/>]
        ]);
        const interval = setInterval(() => {
            try {
                formItems.forEach((item, index) => {
                    const key: string = `key/form-value/${index}`
                    return RenderPromptForm(key, ...item, map);
                });
            } catch (e) {
                console.error(e);
                clearInterval(interval);
            }
            clearInterval(interval);
        }, 600);
        return () => clearInterval(interval);
    }, [location, formsValues]);
    useEffect(() => {
        apiGetProjects().then((response: any[]) => {
            return apiGetProjectsForm(response.map(proj => proj.ID));
        }).then(formList => {
            const formValues = formList
                .flatMap(form => form.tabs)
                .flatMap(tab => tab.groups)
                .flatMap(group => group.fields.map(
                        (field: { label: string, value: any }) => (
                            {
                                label: field.label,
                                value: field.value
                            }
                        )
                    )
                );
            setFormValues(formValues);
        })
    }, []);
    useEffect(() => {
        const interval = setInterval(() => {
            try {
                const element = document.querySelector(`[id="file116"]`) as HTMLElement;
                const root = document.createElement('div');
                root.style.position = "absolute";
                root.style.bottom = "0";
                root.style.right = "-224px";
                element.parentElement?.appendChild(root);
                if (element && element.parentElement) RenderElement(root, (
                    <ImageInfo/>
                ))
            } catch (e) {
                clearInterval(interval);
            }
            clearInterval(interval);
        }, 600);
        return () => clearInterval(interval);
    }, []);
    return (
        <Button
            type={"primary"}
            shape="circle"
            size={"large"}
            icon={<InfoOutlined/>}
            onClick={() => {
                setCurrent(5);
                setStartingTour(true);
            }}
        />
    )
}