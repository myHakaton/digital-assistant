import React, {useEffect} from "react";
import {InfoOutlined} from "@ant-design/icons";
import {Button} from "antd";
import {useSetRecoilState} from "recoil";
import {currentStepHelp, startingStatusHelp} from "../state/state.atoms";
import {useInit} from "../state/state.hooks";

export const EventsRequest: React.FC = () => {
    const setStartingTour = useSetRecoilState(startingStatusHelp);
    const setCurrent = useSetRecoilState(currentStepHelp);
    const init = useInit();
    useEffect(() => {
        init();
    }, []);
    return (
        <Button
            type={"primary"}
            shape="circle"
            size={"large"}
            icon={<InfoOutlined/>}
            onClick={() => {
                setCurrent(8);
                setStartingTour(true);
            }}
        />
    )
}