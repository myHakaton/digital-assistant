import React from 'react'
import ReactDOM from 'react-dom/client'
import {App} from "./app";
import {ConfigProvider} from "antd";
import {RouterProvider} from "react-router";
import {createBrowserRouter} from "react-router-dom";
import {EventsPage} from "./page/events";
import {EventPage} from "./page/event";
import {RootPage} from "./page/root";
import {RecoilRoot} from "recoil";
import ruRU from 'antd/locale/ru_RU';
import {ProjectsCreate} from "./page/projects-create";
import {EventsRequest} from "./page/events-request";

const router = createBrowserRouter([
    {
        path: "/",
        element: <App/>,
        children: [{
            path: "/",
            element: <RootPage/>
        }, {
            path: "events",
            element: <EventsPage/>
        }, {
            path: "events/:id",
            element: <EventPage/>
        },{
            path: "events/:id/request/:requestId",
            element: <EventsRequest/>,
        },{
            path: "projects/create/:id",
            element: <ProjectsCreate/>,
        },{
            path: "projects/edit/:id",
            element: <ProjectsCreate/>,
        }, {
            path: "*",
            element: "other"
        }]
    },
]);

function delay() {
    setTimeout(function () {
        const elementRoot: HTMLElement = document.getElementById('app')?.parentElement as HTMLElement;
        const myElement = document.createElement('div');
        myElement.setAttribute("id", "root");
        myElement.style.position = "absolute";
        myElement.style.top = "0";
        myElement.style.right = "0";
        myElement.style.height = "100vh";
        myElement.style.width = "50px";
        elementRoot.appendChild(myElement);
        ReactDOM.createRoot(myElement).render(
            <ConfigProvider locale={ruRU} theme={{
                token: {
                    colorPrimary: "#faa419",
                    colorPrimaryHover: "#e19417",
                    colorTextLightSolid: "#fff"
                }
            }}>
                <RecoilRoot>
                    <RouterProvider router={router}/>
                </RecoilRoot>
            </ConfigProvider>
        )
    }, 200);
}

if (document.readyState == 'complete') {
    delay();
} else {
    document.onreadystatechange = function () {
        if (document.readyState === "complete") {
            delay();
        }
    }
}
