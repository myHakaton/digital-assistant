import { atom, AtomEffect } from "recoil";

const localStorageBoolEffect = (key: string): AtomEffect<boolean> => ({ setSelf, onSet }) => {
    const savedValue = localStorage.getItem(key)
    if (!!savedValue) {
        setSelf(true);
    }

    onSet((newValue, _, isReset) => {
        isReset
            ? localStorage.removeItem(key)
            : localStorage.setItem(key, newValue ? "start" : "");
    });
};

const localStorageNumberEffect = (key: string): AtomEffect<number> => ({ setSelf, onSet }) => {
    const savedValue = localStorage.getItem(key)
    if (savedValue != null) {
        setSelf(parseInt(savedValue));
    }

    onSet((newValue, _, isReset) => {
        isReset
            ? localStorage.removeItem(key)
            : localStorage.setItem(key, String(newValue));
    });
};

export const startingStatusHelp = atom<boolean>({
    key: 'startingStatusHelp',
    default: Boolean(localStorage.getItem("startingStatusHelp")),
    effects: [
        localStorageBoolEffect('startingStatusHelp'),
    ]
});

export const currentStepHelp = atom<number>({
    key: 'currentStepHelp',
    default: Number(localStorage.getItem("currentStepHelp")),
    effects: [
        localStorageNumberEffect('currentStepHelp'),
    ]
});

export const blockSound = atom<boolean>({
    key: 'blockSound',
    default: Boolean(localStorage.getItem('blockSound')),
    effects: [localStorageBoolEffect('blockSound')]
});