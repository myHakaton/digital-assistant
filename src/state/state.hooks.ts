import {useRecoilCallback, useRecoilValue, useSetRecoilState} from "recoil";
import {useLocation, useNavigate} from "react-router";
import {currentStepHelp, startingStatusHelp} from "./state.atoms";
import {useCallback, useEffect} from "react";

export const useSetCurrent = () => {
    const setCurrent = useSetRecoilState(currentStepHelp);
    const navigate = useNavigate();
    const location = useLocation();
    return useRecoilCallback(() => (current: number): void => {
        setCurrent(current);
        switch (current) {
            case 0: {
                if (location.pathname !== "/" && location.pathname !== "/events") {
                    navigate("/");
                    navigate(0);
                }
                break;
            }
            case 1:
            case 2: {
                if (location.pathname !== "/events") {
                    navigate("/events");
                    navigate(0);
                }
                break;
            }
            case 3:
            case 4:
            case 7: {
                if (location.pathname !== "/events/c490a68f-9354-40b0-8f81-61978d48ccff") {
                    navigate("/events/c490a68f-9354-40b0-8f81-61978d48ccff");
                    navigate(0);
                }
                break;
            }
            case 5:
            case 6: {
                if (location.pathname !== "/projects/create/4e35bdc8-3527-4400-876b-602d3f94b3aa:6539d724-be8f-46d9-86ae-9d152a0d1b1d") {
                    navigate("/projects/create/4e35bdc8-3527-4400-876b-602d3f94b3aa:6539d724-be8f-46d9-86ae-9d152a0d1b1d");
                    navigate(0);
                }
                break;
            }
            case 8: {
                if (location.pathname !== "/events/c490a68f-9354-40b0-8f81-61978d48ccff/request/8332b12f-a3fe-48f0-8e04-e594a0ec2fd7:7238b271-a6aa-4d5d-8ad7-4669d31a5d7c:groups:grant") {
                    navigate("/events/c490a68f-9354-40b0-8f81-61978d48ccff/request/8332b12f-a3fe-48f0-8e04-e594a0ec2fd7:7238b271-a6aa-4d5d-8ad7-4669d31a5d7c:groups:grant");
                    navigate(0);
                }
                break;
            }
        }
    }, [location]);
};

export const useInit = () => {
    const startingTour = useRecoilValue(startingStatusHelp);
    const init = useSetCurrent();
    const current = useRecoilValue(currentStepHelp);
    return useCallback(() => {
        if (startingTour) {
            init(current);
        }
    }, [current, init, startingTour])
}