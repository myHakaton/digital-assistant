import {AudioMutedOutlined, AudioOutlined} from "@ant-design/icons";
import {Button} from "antd";
import React, {useEffect, useState} from "react";
import {Outlet, useLocation, useNavigate} from "react-router";
import {useSetRecoilState} from "recoil";
import {recognize} from "./lib/speech-recognition";
import {startingStatusHelp} from "./state/state.atoms";
import {TourHelp} from "./tour-help/tour-help";

var startTourKeyword = 'Включи подсказку.';

const MicButton = () => {
    const [isRecognize, setIsRecognize] = useState(false)
    const setStartingTour = useSetRecoilState(startingStatusHelp);
    return <Button
        style={{marginTop: 5}}
        type={"primary"}
        shape="circle"
        size={"small"}
        icon={isRecognize ? <AudioOutlined/> : <AudioMutedOutlined/>}
        onClick={() => {
            setIsRecognize(true)
            recognize()
                .then(res => res === startTourKeyword)
                .then(setStartingTour)
                .then(() => setIsRecognize(prev => !prev))
        }}
    />
}

export const App: React.FC = () => {
    const nav = useNavigate();
    const loc = useLocation();
    useEffect(() => {
        let url = location.href;
        const pathname = new URL(url).pathname;
        document.body.addEventListener('click', () => {
            requestAnimationFrame(() => {
                if (url !== location.href) {
                    const pathname = new URL(location.href).pathname;
                    nav(pathname);
                }
                url = location.href;
            });
            if (loc.pathname !== pathname) {
                nav(pathname);
            }
        }, true);
    }, []);
    return (
        <div style={{
            position: "sticky", top: 70, right: 0, marginLeft: 8, zIndex: 1001,
            alignItems: "center", display: "flex", flexDirection: "column"
        }}>
            <Outlet/>
            <MicButton/>
            <TourHelp/>
        </div>
    )
}