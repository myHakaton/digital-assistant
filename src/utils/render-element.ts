import React, {ReactElement} from "react";
import ReactDOM from "react-dom/client";

export const RenderElement: (element: HTMLDivElement, component: ReactElement) => void = (element: HTMLDivElement, component: ReactElement) => {
    ReactDOM.createRoot(element).render(component);
}