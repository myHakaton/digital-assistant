import {RenderElement} from "./render-element";
import React from "react";
import {Popover, Space, Typography} from "antd";
import {InfoCircleFilled} from "@ant-design/icons";

export const RenderPromptForm: (key: string, title: string, prompt: string, map: Map<string, JSX.Element>) => HTMLElement | null = (key: string, title: string, prompt: string, map: Map<string, JSX.Element>): HTMLElement | null => {
    const element = document.querySelector(`[title="${title}"]`) as HTMLElement;
    if (element &&  element.parentElement) {
        const child = element.parentElement.querySelector(`[title="${key}"]`);
        if (child) child.remove();
        const root = document.createElement('div');
        root.setAttribute("title", key);
        element.parentElement?.appendChild(root);
        RenderElement(root,
            <Space>
                <Popover
                    content={map.get(key)}
                    trigger={"click"}
                    overlayStyle={{
                        width: 400
                    }}
                >
                    <InfoCircleFilled style={{color: "#faa419"}}/>
                </Popover>
                <Typography.Text type="secondary" strong>
                    Пример:
                </Typography.Text>
                <Typography.Text type="secondary">
                    {prompt}
                </Typography.Text>
            </Space>
        );
    }
    return element;
}