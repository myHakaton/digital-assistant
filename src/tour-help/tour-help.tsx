import React, {ReactNode, useEffect, useState} from "react";
import {Button, Drawer, Space, Tour, Typography} from "antd";
import {useRecoilValue, useSetRecoilState} from "recoil";
import {blockSound, currentStepHelp, startingStatusHelp} from "../state/state.atoms";
import {useSetCurrent} from "../state/state.hooks";
import {ruSpeach} from "../lib/speech-synthesis";
import {PauseOutlined, PlayCircleOutlined, SoundOutlined} from "@ant-design/icons";

const events = 'На вкладке мероприятия, вы можете ознакомится со списком всех мероприятий которые могут вас заинтересовать.'

const searchEvent = 'Используя фильтры и строку поиска, вы можете найти мероприятиее которое наиболее вам подойдет.'

const eventCard = 'Нажмите на карточку мероприятия для того, что бы более детально ознакомиться с мероприятием'

const fullEventInfo = 'Здесь вы можете ознакомится с детальной информацией о мероприятии.'

const crateProject = 'Если мероприятие вас заинтересовало, вы можете перейти по ссылки для создания проекта, либо запомнить название шаблона для того что бы использовать его на странице создания проектов.'

const projectInfo = ' На данной форме необходимо заполнить информацию о проекте.'

const saveButtonDraft = 'Кнопка сохранение черновика, позволяет в любой момент сохранить черновой вариант проекта.'
const saveButton = 'СОХРАНЕНИЕ ЧЕРНОВИКА НЕ ОБНОВЛЯЮТ ДАННЫЕ В ЗАЯВКЕ НА ГРАНТ, ДЛЯ ЭТОГО НЕОБХОДИМО НАЖАТЬ КНОПКУ СОХРАНИТЬ ПРОЕКТ'

const fillingButton = 'После сохранения итогового варианта проекта (Нажатием кнопки сохранить проект), вы можете приступить к подаче заявки на грант.'

const filling = 'Необходимо выбрать проект и заполнить необходимую информацию. После этого подать заявку на грант.'

const steps = [events, searchEvent, eventCard, fullEventInfo, crateProject, projectInfo, saveButtonDraft + ' ' + saveButton, fillingButton, filling]

const BlockSound = () => {
    const isSoundBlocked = useRecoilValue(blockSound);
    const setIsSoundBlocked = useSetRecoilState(blockSound);
    return <Button
        icon={isSoundBlocked ? <PauseOutlined/> : <SoundOutlined/>}
        size={"small"}
        onClick={() => setIsSoundBlocked((prev) => !prev)}
    />
}

type TourCardType = { children: ReactNode, link: ReactNode }

const TourCard = ({children, link}: TourCardType) => {
    return <Space direction={"vertical"}>
        {children}
        {link}
    </Space>
}

const Title: React.FC<{ title: string }> = (props) => {
    return (
        <Space direction={"horizontal"} split={<Drawer/>}>
            <div>{props.title}</div>
            <BlockSound/>
        </Space>
    )
}

export const TourHelp: React.FC = () => {
    const startingTour = useRecoilValue(startingStatusHelp);
    const isSoundBlocked = useRecoilValue(blockSound);
    const setStartingTour = useSetRecoilState(startingStatusHelp);
    const current = useRecoilValue(currentStepHelp);
    const onChange = useSetCurrent();
    const [elementRef, setElementRef] = useState<HTMLElement | null>(null);
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        const timeout = setTimeout(() => {
            speechSynthesis.cancel()
            if (steps[current] && startingTour && !isSoundBlocked) {
                speechSynthesis.cancel()
                ruSpeach.text = steps[current]
                speechSynthesis.speak(ruSpeach);
            }
        }, 300)
        return () => clearTimeout(timeout)
    }, [current, startingTour, isSoundBlocked])
    useEffect(() => {
        if (startingTour) {
            setLoading(true);
            const interval = setInterval(() => {
                let element: HTMLElement | undefined;
                switch (current) {
                    case 0:
                        element = document.querySelectorAll('[href*="/events"]')[0] as HTMLElement;
                        break;
                    case 1:
                        element = document.getElementsByClassName('app-tabs_row_1BFHF app-tabs_filters_3dT5G')[0] as HTMLElement;
                        break;
                    case 2:
                        element = document.getElementsByClassName('base-card base-card events-card_card_14u0c')[0] as HTMLElement;
                        break;
                    case 3:
                        element = document.getElementsByClassName('event-view_list_34NS2')[0] as HTMLElement;
                        break;
                    case 4:
                        element = document.querySelectorAll(`[href*="/projects/create/4e35bdc8-3527-4400-876b-602d3f94b3aa:6539d724-be8f-46d9-86ae-9d152a0d1b1d"]`)[0]?.parentElement?.parentElement as HTMLElement;
                        break;
                    case 5:
                        element = document.getElementsByClassName('base-tabs__list')[0] as HTMLElement;
                        break;
                    case 6:
                        element = document.getElementsByClassName('project-edit-form_footer_2gbME')[0] as HTMLElement;
                        break;
                    case 7:
                        element = document.getElementsByClassName('base-button base-button--primary event-view-part_button_2o80l')[0] as HTMLElement;
                        break;
                    case 8:
                        element = document.getElementsByClassName('base-row base-row__separator base-row__separator--bottom event-request_row_3t5ut')[0] as HTMLElement;
                        break;
                }
                if (element) {
                    setElementRef(element);
                    clearInterval(interval);
                    setLoading(false);
                }
            }, 1);

        }
    }, [startingTour, current]);
    if (loading) {
        return null;
    }
    return (
        <Tour
            open={startingTour}
            current={current}
            onClose={() => {
                setStartingTour(false);
            }}
            onChange={onChange}
            steps={[
                {
                    title: <Title title={'Вкладка мероприятия'}/>,
                    description: (
                        <TourCard
                            link={<Typography.Link href="https://academy.myrosmol.ru/" target="_blank">
                                Научим, как оформить и подать заявку грантового конкурса.
                            </Typography.Link>}
                        >
                            <Typography.Text>
                                {events}
                            </Typography.Text>
                        </TourCard>
                    ),
                    target: elementRef
                },
                {
                    title: <Title title={'Поиск необходимого мероприятия'}/>,
                    description: (
                        <TourCard
                            link={<Typography.Link href="https://academy.myrosmol.ru/" target="_blank">
                                Научим, как оформить и подать заявку грантового конкурса.
                            </Typography.Link>}
                        >
                            <Typography.Text>
                                {searchEvent}
                            </Typography.Text>
                        </TourCard>
                    ),
                    target: elementRef
                },
                {
                    title: <Title title={'Карточка мероприятия'}/>,
                    description: (
                        <TourCard
                            link={<Typography.Link href="https://academy.myrosmol.ru/" target="_blank">
                                Научим, как оформить и подать заявку грантового конкурса.
                            </Typography.Link>}
                        >
                            <Typography.Text>
                                {eventCard}
                            </Typography.Text>
                        </TourCard>
                    ),
                    target: elementRef
                },
                {
                    title: <Title title={'Полная ифнормация о мероприятии'}/>,
                    description: (
                        <TourCard
                            link={<Typography.Link href="https://academy.myrosmol.ru/" target="_blank">
                                Научим, как оформить и подать заявку грантового конкурса.
                            </Typography.Link>}
                        >
                            <Typography.Text>
                                {fullEventInfo}
                            </Typography.Text>
                        </TourCard>
                    ),
                    target: elementRef
                },
                {
                    title: <Title title={'Создание проекта'}/>,
                    description: (
                        <TourCard
                            link={<Typography.Link href="https://academy.myrosmol.ru/" target="_blank">
                                Научим, как оформить и подать заявку грантового конкурса.
                            </Typography.Link>}
                        >
                            <Typography.Text>
                                {crateProject}
                            </Typography.Text>
                        </TourCard>
                    ),
                    target: elementRef
                },
                {
                    title: <Title title={'Форма создания проекта'}/>,
                    description: (
                        <TourCard
                            link={<Typography.Link href="https://academy.myrosmol.ru/" target="_blank">
                                Научим, как оформить и подать заявку грантового конкурса.
                            </Typography.Link>}
                        ><Typography.Text>
                            {projectInfo}
                        </Typography.Text>
                        </TourCard>
                    ),
                    target: elementRef
                },
                {
                    title: <Title title={'Кнопки сохранения'}/>,
                    description: (
                        <TourCard
                            link={<Typography.Link href="https://academy.myrosmol.ru/" target="_blank">
                                Научим, как оформить и подать заявку грантового конкурса.
                            </Typography.Link>}
                        >
                            <Typography.Text>
                                {saveButtonDraft}
                            </Typography.Text>
                            <Typography.Text strong>
                                {saveButton}
                            </Typography.Text>
                        </TourCard>
                    ),
                    target: elementRef
                },
                {
                    title: <Title title={"Кнопка подачи заявки"}/>,
                    description: (
                        <TourCard
                            link={<Typography.Link href="https://academy.myrosmol.ru/" target="_blank">
                                Научим, как оформить и подать заявку грантового конкурса.
                            </Typography.Link>}
                        >
                            <Typography.Text>
                                {fillingButton}
                            </Typography.Text>
                        </TourCard>
                    ),
                    target: elementRef
                },
                {
                    title: <Title title={"Подача заявки"}/>,
                    description: (
                        <TourCard
                            link={<Typography.Link href="https://academy.myrosmol.ru/" target="_blank">
                                Научим, как оформить и подать заявку грантового конкурса.
                            </Typography.Link>}
                        >
                            <Typography.Text>
                                {filling}
                            </Typography.Text>
                        </TourCard>
                    ),
                    target: elementRef
                }
            ]}
        />
    )
}