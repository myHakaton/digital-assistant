// @ts-ignore
var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition
// @ts-ignore
var SpeechGrammarList = SpeechGrammarList || window.webkitSpeechGrammarList
// @ts-ignore
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent

var keywords = ['Включи подсказку'];

var recognition = new SpeechRecognition();
if (SpeechGrammarList) {
    var speechRecognitionList = new SpeechGrammarList();
    var grammar = '#JSGF V1.0; grammar colors; public <color> = ' + keywords.join(' | ') + ' ;'
    speechRecognitionList.addFromString(grammar, 1);
    recognition.grammars = speechRecognitionList;
}
recognition.continuous = false;
recognition.lang = 'ru-RU';
recognition.interimResults = false;
recognition.maxAlternatives = 1;


// recognition.onspeechend = function () {
//     recognition.stop();

// }
export const recognize = () => new Promise((resolve) => {
    recognition.start();
    recognition.onresult = function (event: any) {
        var command = event.results[0][0].transcript;
        console.log(command)
        console.log('Confidence: ' + event.results[0][0].confidence);
        resolve(command)
    }
    recognition.onspeechend = function () {
        recognition.stop();

    }
})
