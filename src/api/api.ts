import axios, {InternalAxiosRequestConfig} from "axios";


export const instance = axios.create();

instance.interceptors.request.use(
    (config: InternalAxiosRequestConfig): any => ({
        ...config,
        headers: {
            ...config.headers,
            Authorization: `Bearer ${localStorage.getItem("user-token")}`,
        },
    }),
    (error: any) => {
        return Promise.reject(error);
    }
);
