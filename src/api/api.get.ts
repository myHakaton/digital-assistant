import {instance} from "./api";
import {AxiosResponse} from "axios";

export const apiGetEvents = async () => {
    const {data} = await instance.get("/api/events?my=&page=1&per-page=12");
    return data
}

export const apiGetProjects = async () => {
    const {data} = await instance.get("/api/projects?my=&expand=template&isArchive=false&page=1&per-page=10");
    return data
}

export const apiGetProjectForm = async (projectId: string) => {
    const {data} = await instance.get(`/api/projects/${projectId}?expand=form`);
    return data.form
}

export const apiGetProjectsForm = async (projectIdList: string[]) => {
    const test = await Promise.all(projectIdList.map(id => apiGetProjectForm(id)));
    return test
}